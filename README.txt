Install R and its libraries listed below.

Run "Rscript main.R" from the console which will compute and print 
results for all programming questions.
If you want to see the graphs, run the plot and ggvis commands in RStudio.

Used libraries:
- readr for reading datasets
- tibble for working with datasets
- e1071 for svm
- parallel for parallelizing computations
- neuralnet for neural networks
- ggvis for plotting graphs
